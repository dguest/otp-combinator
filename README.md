OTP Combinator
==============

Because combinator sounds cool.

This is a simple script to deal with merging Google survey output into
the larger OTP "master" spreadsheet.

The expected inputs are a CSV file from a google forum, and a chunk of
the OTP master list. Some notes on this:

- The fields for the google forum are all hardcoded. You might need to
  modify them from year to year, or if you change the forum.

- The last 6 columns in the chunk of the master list must correspond
  to the last 6 months.

- The script will print out the names of new people who are missing
  from the master sheet, you should add these by hand and re-export
  the CSV chunk to be sure!

Assuming you have the required CSV files, you can run

```
./combine.py master-chunk.csv forum-data.csv -o new-master-chunk.csv
```

Then import `new-master-chunk.csv` into a sheet, and paste it into the
master list.
