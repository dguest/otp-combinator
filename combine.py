#!/usr/bin/env python3

from argparse import ArgumentParser
from pathlib import Path
import csv, sys
from datetime import datetime as dt

# hardcoded fields from the google forum
MONTHS_1 = ['July', 'August', 'September', 'October', 'November', 'December']
MONTHS_2 = ['January', 'February', 'March', 'April', 'May', 'June']
ID_HEAD = 'OTP ID'
NAME = 'Your Full Name'

def get_args():
    parser = ArgumentParser()
    parser.add_argument('master', type=Path)
    parser.add_argument('new', type=Path)
    parser.add_argument('-o', '--outfile', required=True, type=Path)
    parser.add_argument('-k', '--keep-zero', action='store_true')
    return parser.parse_args()

def get_id_field(reader):
    example = next(reader)
    for k in example.keys():
        if k.startswith(ID_HEAD):
            return k
    raise ValueError('no otp id found in reader')

def run():
    args = get_args()
    months = MONTHS_2 if 4 <= dt.now().month < 10 else MONTHS_1
    with open(args.new) as new:
        reader = csv.DictReader(new)
        otp_id = get_id_field(reader)
        id_dic = {int(r[otp_id]): r for r in reader}
    seen_ids = set()
    with open(args.master) as ifile, open(args.outfile,'w') as ofile:
        writer = csv.writer(ofile)
        for row in csv.reader(ifile):
            otpid = int(row[0])
            seen_ids.add(otpid)
            if up := id_dic.get(otpid):
                last6 = [float(up[m]) if up[m] else 0 for m in months]
                row[-6:] = [f'{m:.2f}' for m in last6]
            fullotp = sum(float(x) for x in row[-12:])
            if fullotp > 0 or args.keep_zero:
                writer.writerow(row)

    if unused := set(id_dic) - seen_ids:
        print('found missing entries!')
        prfields = NAME, otp_id
        for otpid in unused:
            name, discr = (id_dic[otpid][n] for n in prfields)
            print(f'{name}: {discr}')

if __name__ == '__main__':
    sys.exit(run())
